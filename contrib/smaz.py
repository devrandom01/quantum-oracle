#!/usr/bin/env python

# test compression with the smaz library
#
# pip install python-smaz

import sys
import pysmaz


def main():
    with open(sys.argv[1], "rb") as f:
        text = f.read()
    print(len(pysmaz.compress(text)))
    print(pysmaz.decompress(pysmaz.compress(text)))


if __name__ == "__main__":
    main()
