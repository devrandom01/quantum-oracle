# Quantum Oracle

Current output in https://quantumoracle.io/

## What is this?

This script reads entropy from the Bitcoin network and attempts to find a [quantum immortality](https://en.wikipedia.org/wiki/Quantum_suicide_and_immortality) oracle.

A quantum immortality oracle is a random message source that generates an intelligible message for some branches in the multiverse where the observer of the message stays alive because they read message.  For example, this may be a message that helps the observer bypass the Great Filter (Robin Hanson / Nick Bostrom).

## Do you expect this to work?

This will produce an output if the following conditions are met:

- The Bitcoin network is sufficiently random
- The many-worlds interpretation of quantum mechanics is correct
- The probability of survival without reading the oracle's message is low
- The probability of survival with reading the oracle's message is high
- The probability of randomly generating the oracle's message is higher than the probability of survival without reading the oracle's message

More precisely, if X is the probability of survival without reading the oracle's message, Y is the probability of survival after reading the oracle's message, and Z is the probability of randomly generating a suitable message, then the following would roughly result in a message being produced with probability > 0.5:

    X < Y * Z

## Other Questions and Observations

- **Can someone insert a message pretending to be an oracle?**
  No, that is not economically feasible.  The cost of inserting a message is the cost of producing many blocks, because the winning block has an unpredictable hash value, and the chance that a hash can be interpreted as a specific chosen text is negligible.  A bounty is available if you manage to do this.
- **Why do I see words in the text, shouldn't that be unlikely?**
  The text is decompressed using a dictionary compression algorithm.  The algorithm will often interpret random data as words.  The interesting part is the intelligibility / relevance of the message, not whether it contains random words.
- **Why bother with the decompression step, rather than just interpreting the raw bytes as the message?**
  The decompression step reduces the amount of entropy required to produce a message, which increases the chance of success.  The downside is that more messages will be produced, which requires more human effort to filter out irrelevant messages.
- Messages are considered potentially intelligible if they have at least 6 words.  Messages below this threshold are discarded.

## How do I use this?

1. Install Python 3
2. Install the dependencies: `pip install -r requirements.txt`
3. Check out [Unishox2](https://github.com/siara-cc/Unishox2) in a sibling directory
4. Run `./build.sh`
5. Run `./qo.py`

## How does this work?

The script reads block hashes from the Bitcoin network and uses them to generate random message bytes.  The message is then decoded from these bytes using a short-text compression algorithm.

## Credits

- This script uses the Blockstream Esplora block explorer API - https://blockstream.info/
- Short string decompression - https://github.com/siara-cc/Unishox2 (`Unishox3_Alpha` directory)

## License

This script is licensed under the Apache License, Version 2.0 - see the [LICENSE](LICENSE) file for details

## TODO

- [ ] add quantum noise to the Bitcoin network via OP_RETURN transactions (it's not clear that this is helpful, there is probably enough noise already)
- [x] post oracle messages to a public forum
- [x] filter using an intelligibility measure
- [x] use a more sophisticated decompression algorithm - e.g. using a dictionary
