The Quantum Oracle reads entropy from the Bitcoin network and attempts to find a [quantum immortality](https://en.wikipedia.org/wiki/Quantum_suicide_and_immortality) oracle.

A quantum immortality oracle is a random message source that generates an intelligible message for some branches in the multiverse where the observer of the message stays alive because they read message.  For example, this may be a message that helps the observer bypass the [Great Filter](https://mason.gmu.edu/~rhanson/greatfilter.html).
