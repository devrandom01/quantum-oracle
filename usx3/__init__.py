import os

import cffi

ffi = cffi.FFI()
ffi.cdef("""
int usx3_encode(const char *in, size_t in_len, char *out, size_t out_len);
int usx3_decode(const char *in, size_t in_len, char *out, size_t out_len);
""")

script_dir = os.path.dirname(os.path.realpath(__file__))
lib = ffi.dlopen(f"{script_dir}/usx3.so")


def encode(text: str) -> bytes:
    in_len = len(text)
    out_len = in_len * 2
    in_ = ffi.new("char[]", text.encode("ascii"))
    out = ffi.new("char[]", out_len)
    actual_len = lib.usx3_encode(in_, in_len, out, out_len)
    if actual_len > out_len:
        raise Exception("Output buffer too small")
    return ffi.unpack(out, actual_len)


def decode(text: bytes) -> str:
    in_len = len(text)
    out_len = 1024*1024
    out = ffi.new("char[]", out_len)
    actual_len = lib.usx3_decode(text, in_len, out, out_len)
    if actual_len > out_len:
        raise Exception("Output buffer too small")
    return ffi.unpack(out, actual_len).decode("unicode_escape")


if __name__ == "__main__":
    enc = encode("hello world")
    print(enc)
    print(decode(enc))

