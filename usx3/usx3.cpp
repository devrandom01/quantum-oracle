#include "Unishox3_Alpha/unishox3.h"
extern "C" {

int usx3_encode(const char *in, size_t in_len, char *out, size_t out_len) {
    unishox3 usx3;
    int clen = usx3.compress(in, in_len, USX3_API_OUT_AND_LEN(out, out_len));
    return clen;
}

int usx3_decode(const char *in, size_t in_len, char *out, size_t out_len) {
    unishox3 usx3;
    int dlen = usx3.decompress(in, in_len, USX3_API_OUT_AND_LEN(out, out_len));
    return dlen;
}

}
