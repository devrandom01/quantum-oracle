import cffi

ffi = cffi.FFI()
ffi.embedding_api("""
int usx3_encode(const char *in, size_t in_len, char *out, size_t out_len);
int usx3_decode(const char *in, size_t in_len, char *out, size_t out_len);
""")

ffi.set_source(
    "_usx3",
    """
""",
    sources=["../../Unishox2/Unishox3_Alpha/unishox3.cpp"],
    includes=["../../Unishox2/Unishox3_Alpha"]
)


if __name__ == "__main__":
    ffi.compile(verbose=True)
