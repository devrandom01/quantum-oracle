#!/usr/bin/env python
import datetime
import html
import os

import requests
import sqlite3
import usx3

from intelligible import TextEval

MAX_REORG_DEPTH = 12
ESPLORA_ENDPOINT = 'https://blockstream.info/api/'
START_HEIGHT = 770000
DEBUG = False


def main():
    te = TextEval()
    db = sqlite3.connect('qo.db')
    cursor = db.cursor()
    cursor.execute('CREATE TABLE IF NOT EXISTS blocks (height INTEGER PRIMARY KEY, block_hash TEXT)')
    db.commit()
    session = requests.Session()
    tip_height = session.get(ESPLORA_ENDPOINT + 'blocks/tip/height').json()
    print("current height: ", tip_height)
    messages = []
    for height in range(START_HEIGHT, tip_height):
        block_hash = get_hash_at_height(height, tip_height, session, db)
        # print("block height: ", i, "block_hash: ", block_hash)
        text = extract_message(height, block_hash)
        if text:
            score, text = te.num_words(text)
            if score >= 6:
                messages.append((height, block_hash, score, text))
                print("height: ", height, "score: ", score, "text: ", text.encode('unicode_escape').decode('ascii'))

    messages.reverse()
    now = datetime.datetime.now()
    ts = now.isoformat(sep=' ', timespec='seconds')
    print(ts)

    # write to _recent.html
    with open('site/src/_recent.html.new', 'w') as f:
        for height, block_hash, score, text in messages[0:20]:
            emit_message(f, block_hash, height, score, text)
        f.write(f'<sub>Last updated: {ts}</sub>\n')
    os.rename('site/src/_recent.html.new', 'site/src/_recent.html')

    with open('site/src/_all.html.new', 'w') as f:
        for height, block_hash, score, text in messages:
            emit_message(f, block_hash, height, score, text)
        f.write(f'<sub>Last updated: {ts}</sub>\n')
    os.rename('site/src/_all.html.new', 'site/src/_all.html')


def emit_message(f, block_hash, height, score, text):
    text = text.encode('unicode_escape').decode('ascii')
    text = html.escape(text)
    text = text.replace('{', '\\{').replace('}', '\\}')

    f.write(f'<div class="message-line"><span class="height">{height}</span>'
            f' <span class="hash">...{block_hash[-8:]}</span>'
            f' <span class="score">{score}</span>'
            f' <span class="message">{text}<span></div>\n')


def extract_message(height, hash):
    bs = bytes.fromhex(hash)[::-1]
    bs = bs.rstrip(b'\x00')
    bs = bs[:-1]
    try:
        text = usx3.decode(bs)
    except Exception as e:
        print("block height: ", height, "error: ", e)
        return None
    return text


def get_hash_at_height(height, tip_height, session, db):
    cursor = db.cursor()
    if height < tip_height - MAX_REORG_DEPTH:
        cursor.execute('SELECT hash FROM blocks WHERE height = ?', (height,))
        row = cursor.fetchone()
        if row:
            return row[0]
    hash = session.get(ESPLORA_ENDPOINT + 'block-height/' + str(height)).text
    # print("block height: ", height, "hash: ", hash)
    cursor.execute('INSERT OR REPLACE INTO blocks VALUES (?, ?)', (height, hash))
    db.commit()
    return hash


if __name__ == "__main__":
    main()
