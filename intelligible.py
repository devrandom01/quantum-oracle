from unittest import TestCase

import contractions
import nltk
from nltk.corpus import words, wordnet


def download():
    nltk.download("words", quiet=True)
    nltk.download("punkt", quiet=True)
    nltk.download('wordnet', quiet=True)



class TextEval:
    def __init__(self):
        download()
        self.words = set(words.words())

    def num_words(self, text):
        text = contractions.fix(text)
        text = nltk.word_tokenize(text)
        text = map(str.lower, text)
        text = [ws for w in text for ws in self.maybe_split(w)]
        # print(list(text))
        good = [word for word in text if len(word) > 2 and self.is_good(word)]
        return len(set(good)), " ".join(text)

    def maybe_split(self, word):
        if self.is_good(word):
            return [word]
        for i in range(3, len(word) - 2):
            if self.is_good(word[:i]) and self.is_good(word[i:]):
                return [word[:i], word[i:]]
        return [word]

    def is_good(self, word):
        return word in self.words or wordnet.morphy(word) is not None


class TestTextEval(TestCase):
    def test_num_words(self):
        text = "And now for something completly different"
        te = TextEval()
        assert te.num_words(text) == 5


class TestMaybeSplit(TestCase):
    def test_maybe_split(self):
        te = TextEval()
        self.assertEqual(te.maybe_split('hello'), ['hello'])
        self.assertEqual(te.maybe_split('hellothere'), ['hello', 'there'])
        self.assertEqual(te.maybe_split('hellothereasdf'), ['hellothereasdf'])
        self.assertEqual(te.maybe_split('something'), ['something'])


def main():
    ws = set(words.words())
    looks = ['deny', 'denied', 'denies', 'Denying', 'asdf', 'i''m']
    print([word in ws for word in looks])
    print([wordnet.morphy(word) for word in looks])
    print([contractions.fix(word) for word in looks])


if __name__ == "__main__":
    download()
    main()
